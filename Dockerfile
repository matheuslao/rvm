
FROM centos:7

LABEL version="0.1"

RUN yum -y install curl which tar && \
    yum clean all                 && \
    rm -rf /var/cache/yum

RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \ 
    \curl -sSL https://get.rvm.io | bash && \
    /bin/bash -l -c "rvm requirements"

ENV PATH /usr/local/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

COPY .ruby-version .ruby-version

RUN rvm get head && \
    rvm reload && \
    rvm install $(cat .ruby-version) --rubygems ignore && \
    /bin/bash -l -c "rvm --default use $(cat .ruby-version) --create" && \
    /bin/bash -l -c "gem update --system --no-ri --no-rdoc" && \
    /bin/bash -l -c "rvm cleanup all"
